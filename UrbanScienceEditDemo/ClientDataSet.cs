﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace UrbanScienceEditDemo
{
    public class ClientDataSet
    {
///        private DataSet DataSetObj { get; set; }
        public static DataSet ReturnDataSet()
        {
            string connectionString = GetConnectionString();

            //Create a SqlConnection to the Northwind database.
            using (SqlConnection connection =
                       new SqlConnection(connectionString))
            {
                //Create a SqlDataAdapter for the Suppliers table.
                SqlDataAdapter adapter = new SqlDataAdapter();

    // A table mapping names the DataTable.
    adapter.TableMappings.Add("Table", "Clients");

                // Open the connection.
                connection.Open();
                Console.WriteLine("The SqlConnection is open.");

                // Create a SqlCommand to retrieve Suppliers data.
                SqlCommand command = new SqlCommand(
                    "SELECT Client FROM dbo.Clients;",
                    connection);
    command.CommandType = CommandType.Text;

                // Set the SqlDataAdapter's SelectCommand.
                adapter.SelectCommand = command;

                // Fill the DataSet.
                DataSet dataSet = new DataSet("Clients");
    adapter.Fill(dataSet);

                // Create a second Adapter and Command to get
                // the Projects table, a child table of Clients. 
                SqlDataAdapter projectsAdapter = new SqlDataAdapter();
    projectsAdapter.TableMappings.Add("Table", "Projects");

                SqlCommand projectsCommand = new SqlCommand(
                    "SELECT Project, ClientName FROM dbo.Projects;",
                    connection);
    projectsAdapter.SelectCommand = projectsCommand;

                // Fill the DataSet.
                projectsAdapter.Fill(dataSet);

                // Close the connection.
                connection.Close();
                Console.WriteLine("The SqlConnection is closed.");

                // Create a DataRelation to link the two tables
                // based on the SupplierID.
                DataColumn parentColumn =
                    dataSet.Tables["Clients"].Columns["Client"];
    DataColumn childColumn =
        dataSet.Tables["Projects"].Columns["ClientName"];
    DataRelation relation =
        new System.Data.DataRelation("ClientsProjects",
        parentColumn, childColumn);
    dataSet.Relations.Add(relation);
                Console.WriteLine(
                    "The {0} DataRelation has been created.",
                    relation.RelationName);
                return dataSet;
            }
        }






        static void Main()
        {
            string connectionString = GetConnectionString();
            ConnectToData(connectionString);
        }





        private static void ConnectToData(string connectionString)
        {
            //Create a SqlConnection to the AutoHook database.
            using (SqlConnection connection =
                       new SqlConnection(connectionString))
            {
                //Create a SqlDataAdapter for the Client table.
                SqlDataAdapter adapter = new SqlDataAdapter();

                // A table mapping names the DataTable.
                adapter.TableMappings.Add("Table", "Clients");

                // Open the connection.
                connection.Open();
                Console.WriteLine("The SqlConnection is open.");

                // Create a SqlCommand to retrieve Clients data.
                SqlCommand command = new SqlCommand(
                    "SELECT Client, Updated, Created FROM dbo.Clients;",
                    connection);
                command.CommandType = CommandType.Text;

                // Set the SqlDataAdapter's SelectCommand.
                adapter.SelectCommand = command;

                // Fill the DataSet.
                DataSet dataSet = new DataSet("Clients");
                adapter.Fill(dataSet);

                // Create a second Adapter and Command to get
                // the Products table, a child table of . 
                SqlDataAdapter productsAdapter = new SqlDataAdapter();
                productsAdapter.TableMappings.Add("Table", "Projects");

                SqlCommand productsCommand = new SqlCommand(
                    "SELECT Project, ClientName FROM dbo.Projects;",
                    connection);
                productsAdapter.SelectCommand = productsCommand;

                // Fill the DataSet.
                productsAdapter.Fill(dataSet);

                // Close the connection.
                connection.Close();
                Console.WriteLine("The SqlConnection is closed.");

                // Create a DataRelation to link the two tables
                // based on the SupplierID.
                DataColumn parentColumn =
                    dataSet.Tables["Clients"].Columns["Clients"];
                DataColumn childColumn =
                    dataSet.Tables["Projects"].Columns["Project"];
                DataRelation relation =
                    new System.Data.DataRelation("ClientsProjects",
                    parentColumn, childColumn);
                dataSet.Relations.Add(relation);
                Console.WriteLine(
                    "The {0} DataRelation has been created.",
                    relation.RelationName);
            }
        }

        static private string GetConnectionString()
        {
            // To avoid storing the connection string in your code, 
            // you can retrieve it from a configuration file.
            return "Data Source = autohook.database.windows.net; Initial Catalog = AutoHook; Persist Security Info = True; User ID = FastidiousWorm; Password = @cuddlePunch91";
        }
    }
}