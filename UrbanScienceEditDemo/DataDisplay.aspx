﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataDisplay.aspx.cs" Inherits="UrbanScienceEditDemo.ImageDisplay" %>

<!DOCTYPE html>


<!–– make the buttons respond ––>
<script runat="server">
    protected void Button1_Click(object sender, System.EventArgs e)
    {
        Label1.Text = "You clicked the first button.";
    }
    void GridView2_RowCommand(Object sender, GridViewCommandEventArgs e)
    {

        // If multiple ButtonField column fields are used, use the
        // CommandName property to determine which button was clicked.
        if(e.CommandName=="ClientButton")
        {

            // Convert the row index stored in the CommandArgument
            // property to an Integer.
            int index = Convert.ToInt32(e.CommandArgument);

            // Get the last name of the selected author from the appropriate
            // cell in the GridView control.
            GridViewRow selectedRow = GridView2.Rows[index];
            TableCell clientName = selectedRow.Cells[1];
            string client = clientName.Text;

            // Display the selected thing.
            ///Message.Text = "You selected " + client + ".";   //way to select specific thing potentially
            Label2.Text = "You clicked the " + client + " button.";

        }

    }

</script>



<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 797px;
            width: 631px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;</div>
        <asp:Label 
            ID="Label1" 
            runat="server" 
            Font-Size="Medium" 
            ForeColor="Black"
            Font-Italic="True"
            />
        <asp:Button ID="Button1" runat="server" Text="Button1" OnClick="Button1_Click" />
        <br />
        <asp:DataList ID="DataList1" runat="server" DataSourceID="DataSetClientsProjects">
        </asp:DataList>
        <asp:ObjectDataSource ID="DataSetClientsProjects" runat="server" SelectMethod="ReturnDataSet" TypeName="UrbanScienceEditDemo.ClientDataSet"></asp:ObjectDataSource>
        <br />
        Clients from ClientsProject DataSet Returned by<br />
        ClientDataSet.cs<asp:GridView ID="GridView1" runat="server" DataSourceID="DataSetClientsProjects">
        </asp:GridView>
        <br />
        <br />
        Projectss from ClientsProject DataSet Returned by<br />
        SQL command via SqlDataSource<br />
        <asp:Label ID="Label2" runat="server" Text="Label2"></asp:Label>
        <br />
        <asp:GridView ID="GridView2" onrowcommand="GridView2_RowCommand" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Client" DataSourceID="SQLSelectClients">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="ClientButton" HeaderText="Customers" DataTextField="Client" Text="Button" DataTextFormatString="{0}" />
                <asp:BoundField DataField="Client" HeaderText="Client" ReadOnly="True" SortExpression="Client" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SQLSelectClients" runat="server" ConnectionString="<%$ ConnectionStrings:AutoHookConnectionString %>" SelectCommand="SELECT [Client] FROM [Clients] ORDER BY [Client] DESC"></asp:SqlDataSource>
    </form>
</body>
</html>
